Well, first of all we understand the importance of your time, so we will keep the introduction short and simple. We have been serving the Canadian Auto Finance Industry since March 2009 and are amongst the very few online lenders in Canada that provide online car repair financing.

Address: 1486 Triole St, Ottawa, ON K1B 3S6, CAN

Phone: 613-902-2509

Website: https://www.carrepairloanscanada.com
